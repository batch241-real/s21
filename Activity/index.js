// console.log("Hello World!");

console.log("Original Array:");

let numArr = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angel', 'Dave Bautista'];
console.log(numArr);

function addArray(addString){
	let addItem = numArr.length;
	numArr[addItem] = addString;
}

addArray("John Cena");
console.log(numArr);


function receiveIndexNum(index){
	return numArr[index];
}

let itemFound = receiveIndexNum(2);
console.log(itemFound);


function deleteArray() {
	let lastItem = numArr.length-1
	console.log(numArr[lastItem]);
	numArr.length = lastItem;
	return numArr;
}

let itemDelete = deleteArray();
console.log(itemDelete);


function updateArray(updateString, index) {
	numArr[index] = updateString;
}

updateArray("Triple H", 3);
console.log(numArr);


function deleteAllItems() {
	numArr = [];
}

deleteAllItems();
console.log(numArr);


function checkUsersEmpty() {
	
	if(numArr > 0){
		return false;
	} else{
		return true;
	}	
}

let isUsersEmpty = checkUsersEmpty();
console.log(isUsersEmpty);