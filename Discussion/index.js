// console.log("Hello World!");

// Arrays
/*
	- An array in programming is simply a list of data.
	- They ae declared using square brackets [], also nown as "Array Literals"
	- They are usually used to store numerous amounts of data to manipulate in order to perform a number of tasks.
	- Arrays provide a number of functions(or methods in other words) that help in achieving this.
	- The main difference of an array to an object is that it contains information in a form of "list", unlike objects that uses properties
	Syntax:
		let/const arrayName  = [elementA, elementB, elementC, etc..]
*/

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

// with Array
let studentNumber = ['2023-1928', '2023-1929', '2023-1930'];
console.log(studentNumber);

// Common examples of an array
let grade = [98.5, 94.3, 99.01, 90.1];
console.log(grade);

let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
console.log(computerBrands);

// Array with mixed data types (not recommended)
let mixedArr = [12, 'Asus', null, undefined, {}];
console.log(mixedArr);


let myTasks = [
	'drink HTML',
	"eat javascript",
	'inhale CSS',
	"bake sass"
];
console.log(myTasks);

// Creating an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// Length Propert
// the .length property allows us to "get" and "set" the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also be used with strings. Some array methods ad properties can be used with strings.
let fullName = "Annejanette Real";
console.log(fullName.length);

// .length property can also set the total number of items in an array. Meaning, we can actually delete the last item in the array or shorten the array by updating its length.
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// Using decrementation
cities.length--;
console.log(cities);

// Not applicable on strings
fullName.length = fullName.length-1;
console.log(fullName);
fullName.length--;
console.log(fullName);

// Adding an item in an array
// If you can shorted the array by setting the length props, we can also lengthen it by adding a number into the length property. But it will be undefined.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// Reading from Arrays
/*
	- We can access array elements through the use of array indexes
	- In JavaScript, the first element is associated with the number 0
	- Array indexes actually refer to an address/location in the device's memory and hot the information is stored.
	Syntax:
		arrayName[index]
*/

console.log(grade[0]);
console.log(computerBrands[1]);

// Accessing an array element that does not exist will return "undefined"
console.log(grade[20]);

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]

// Access the second item in the array
console.log(lakersLegend[1]);

// Access the fourt item in the array
console.log(lakersLegend[3]);

// You can save or store array items in another variable
let currentLaker = lakersLegend[2];
console.log(currentLaker);


// You can re-assign array values using the item's indices
console.log("Array before assignment:");
console.log(lakersLegend);
lakersLegend[2] = "Paul Gasol";
console.log("After reassignment:");
console.log(lakersLegend);

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items into the Array
// Using indices we can add items into the array
let newArr = [];
console.log(newArr[0]); //undefined

newArr [0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]); //undefined
newArr[1] = "Tifa Lockhart"
console.log(newArr);


// We can add items at the end of the array instead of adding it in the front to avoid the risk of replacing the first items in the array.

newArr[newArr.length] = "Anne Janette";
console.log(newArr);

// Looping over an Array
// You can use a for loop to iterate over all items in an array.

for(let index = 0; index < newArr.length; index++){
	console.log(newArr.length)
}

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] +" is divisible by 5");
	} else {
		console.log(numArr[index] +" is not divisible by 5");
	}
}


// Multidimensional Arrays
/*
	Multidimensional arrays are useful for storing complex data structure
*/

